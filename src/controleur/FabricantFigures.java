package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventListener;

import application.Fenetre;
import vue.DessinFigures;
import modele.Carre;
import modele.Cercle;
import modele.Ellipse;
import modele.FigureColoree;
import modele.FigureQuelconque;
import modele.Losange;
import modele.Parallelogramme;
import modele.Point;
import modele.Rectangle;
import modele.Triangle;


/**
 * Classe implementant la creation des figures geometriques via des clics de souris
 */
public class FabricantFigures implements MouseListener, EventListener {

	
	/**
	 * Figure coloree en cours de fabrication
	 */
	private FigureColoree figure_en_cours_de_fabrication;
	
	
	/**
	 * Nombre de clics de souris
	 */
	private int nb_points_cliques;
	
	
	/**
	 * Tableau contenant des points crees par des clics de souris
	 */
	private Point[] points_cliques;
	
	
	/**
	 * Constructeur d'un FabricantFigures
	 * 
	 * @param fc
	 * 			Figure coloree a fabriquer
	 */
	public FabricantFigures(FigureColoree fc) {
		figure_en_cours_de_fabrication = fc;
		nb_points_cliques = 0;
		points_cliques = new Point[fc.nbPoints()];
	}
	
	
	public void mouseClicked(MouseEvent e) {
		
	}

	
	/**
	 * Methode appelee lorsqu'un bouton de souris est presse
	 * 
	 * @param e
	 * 			Action de souris
	 */
	public void mousePressed(MouseEvent e) {
	
		//On teste que le clic n'est pas situe au niveau du PanneauChoix
		if(e.getY() > 105) {
			
			Point p0 = new Point(e.getX(), e.getY());				
			points_cliques[nb_points_cliques] = p0;
			nb_points_cliques++;
		
		}
		
	}

	
	/**
	 * Methode appelee lorsqu'un bouton de souris est relache
	 * 
	 * @param e
	 * 			Action de souris
	 */
	public void mouseReleased(MouseEvent e) {
		
		DessinFigures src = (DessinFigures) e.getSource();
		FigureColoree new_fig = null;

		
		//Si la figure a fabriquer est un rectangle
		if ((figure_en_cours_de_fabrication instanceof Rectangle) && (nb_points_cliques == 1)) {
			
			Point p2 = new Point(e.getX(), e.getY());
			Point p1 = new Point(p2.rendreX(),points_cliques[0].rendreY());
			Point p3 = new Point(points_cliques[0].rendreX(), p2.rendreY());
			
			points_cliques[1] = p1;
			points_cliques[2] = p2;
			points_cliques[3] = p3;
			
			//On modifie les points de la figure et on l'ajoute
			figure_en_cours_de_fabrication.modifierPoints(points_cliques);
			src.ajoute(figure_en_cours_de_fabrication);
			
			new_fig = new Rectangle();
			new_fig.changerCouleur(Fenetre.COULEUR_COURANTE);
			src.supprimerAuditeurs();
			src.construit(new_fig);
			
		}
		
		
		//Si la figure a fabriquer est un triangle
		if((figure_en_cours_de_fabrication instanceof Triangle) && (nb_points_cliques == 3)) {
			
			
			//Modification des points de la figure et on l'ajoute
			figure_en_cours_de_fabrication.modifierPoints(points_cliques);
			src.ajoute(figure_en_cours_de_fabrication);
			
			new_fig = new Triangle();
			new_fig.changerCouleur(Fenetre.COULEUR_COURANTE);
			src.supprimerAuditeurs();
			src.construit(new_fig);
			
		}
		
		
		////Si la figure a fabriquer est un carre
		if((figure_en_cours_de_fabrication instanceof Carre) && (nb_points_cliques == 1)) {
			
			Point cour = new Point(e.getX(),e.getY());
			Point tmp = new Point(cour.rendreX(),points_cliques[0].rendreY());
			
			int cote = (int)points_cliques[0].distance(tmp);
			
			Point p1,p2,p3;
			
			
			//Si on remonte vers le haut
			if(cour.rendreY() < points_cliques[0].rendreY()) {
				Point inversion = cour;
				cour = tmp;
				tmp = inversion;
				points_cliques[0] = new Point(points_cliques[0].rendreX(),points_cliques[0].rendreY()-cote);
			}
			
			
			//Si on va vers la gauche
			if(cour.rendreX() > points_cliques[0].rendreX()) {
				
				p1 = new Point(points_cliques[0].rendreX()+cote,points_cliques[0].rendreY());
				p2 = new Point(p1.rendreX(),p1.rendreY()+cote);
				p3 = new Point(points_cliques[0].rendreX(),points_cliques[0].rendreY()+cote);
				
			} else {
				
				p1 = points_cliques[0];
				p2 = new Point(p1.rendreX(),p1.rendreY()+cote);
				p3 = new Point(p2.rendreX()-cote,p2.rendreY());
				points_cliques[0] = new Point(p3.rendreX(),p1.rendreY());
				
			}
			
			points_cliques[1] = p1;
			points_cliques[2] = p2;
			points_cliques[3] = p3;
			
			
			//Modification des points de la figure
			figure_en_cours_de_fabrication.modifierPoints(points_cliques);
			src.ajoute(figure_en_cours_de_fabrication);
			
			new_fig = new Carre();
			new_fig.changerCouleur(Fenetre.COULEUR_COURANTE);
			src.supprimerAuditeurs();
			src.construit(new_fig);
			
		}
		
		
		////Si la figure a fabriquer est un losange
		if((figure_en_cours_de_fabrication instanceof Losange) && (nb_points_cliques == 2)) {
			
			Point tmp = new Point(points_cliques[0].rendreX(),points_cliques[1].rendreY());
			Point p2 = new Point(points_cliques[0].rendreX(),(int)(tmp.rendreY()+tmp.distance(points_cliques[0])));
			
			//Si on decide de partir vers la gauche
			if(points_cliques[0].rendreX() > points_cliques[1].rendreX()) {
				points_cliques[1] = new Point((int)(tmp.rendreX()+tmp.distance(points_cliques[1])),points_cliques[1].rendreY());
			}
			
			Point p3 = new Point((int)(tmp.rendreX()-tmp.distance(points_cliques[1])), tmp.rendreY());
			
			
			//On teste si l'ordonnee du deuxieme point clique est inferieure a l'ordonnee du premier point clique
			if(points_cliques[1].rendreY() < points_cliques[0].rendreY()) {
				p2 = points_cliques[0];
				points_cliques[0] = new Point(p2.rendreX(),(int)(tmp.rendreY()-tmp.distance(p2)));
			}
			
			points_cliques[2] = p2;
			points_cliques[3] = p3;
			
			
			//On teste si le point le plus haut ne se place pas en-dessus du PanneauChoix
			if(points_cliques[0].rendreY() < 105) {
				Point haut = points_cliques[0];
				points_cliques[0] = new Point(points_cliques[0].rendreX(),106);
				points_cliques[2] = new Point(points_cliques[2].rendreX(),(int)(points_cliques[2].rendreY()-haut.distance(points_cliques[0])));
			}
			
			
			//Modification des points de la figure
			figure_en_cours_de_fabrication.modifierPoints(points_cliques);
			src.ajoute(figure_en_cours_de_fabrication);
			
			new_fig = new Losange();
			new_fig.changerCouleur(Fenetre.COULEUR_COURANTE);
			src.supprimerAuditeurs();
			src.construit(new_fig);
			
		}
		
		
		//Si la figure a fabriquer est une figure quelconque
		if((figure_en_cours_de_fabrication instanceof FigureQuelconque) && (nb_points_cliques == figure_en_cours_de_fabrication.nbPoints())) {
			
			
			//Modification des points de la figure
			figure_en_cours_de_fabrication.modifierPoints(points_cliques);
			src.ajoute(figure_en_cours_de_fabrication);
			
			new_fig = new FigureQuelconque(figure_en_cours_de_fabrication.nbPoints());
			new_fig.changerCouleur(Fenetre.COULEUR_COURANTE);
			src.supprimerAuditeurs();
			src.construit(new_fig);
			
		}
		
		
		//Si la figure a fabriquer est un parallelogramme
		if((figure_en_cours_de_fabrication instanceof Parallelogramme) && (nb_points_cliques == 3)) {
			
			Point p1 = new Point(points_cliques[1].rendreX(),points_cliques[0].rendreY());
			Point tmp = new Point(points_cliques[2].rendreX(),p1.rendreY());
			Point p3 = new Point((int)(points_cliques[2].rendreX()-points_cliques[0].distance(p1)), (int)(p1.rendreY()+tmp.distance(points_cliques[2])));
			
			
			//Si on remonte vers le haut
			if(points_cliques[2].rendreY() < points_cliques[0].rendreY()) {
				
				//Si on va vers la gauche
				if(points_cliques[1].rendreX() > points_cliques[0].rendreX()) {
					
					p3 = points_cliques[0];
					p1 = points_cliques[2];
					points_cliques[2] = new Point(points_cliques[1].rendreX(),p3.rendreY());
					points_cliques[0] = new Point((int)(p1.rendreX()-p3.distance(points_cliques[2])),p1.rendreY());
					
				} else {
					
					Point inversion = points_cliques[2];
					points_cliques[2] = points_cliques[0];
					points_cliques[0] = inversion;
					p3 = new Point(points_cliques[1].rendreX(),points_cliques[2].rendreY());
					p1 = new Point((int)(points_cliques[0].rendreX()+points_cliques[2].distance(p3)),points_cliques[0].rendreY());
					
				}
				
			} else {
				
				//Si on va vers la droite
				if(points_cliques[1].rendreX() < points_cliques[0].rendreX()) {
					
					p1 = points_cliques[0];
					points_cliques[0] = new Point(points_cliques[1].rendreX(),p1.rendreY());
					p3 = points_cliques[2];
					points_cliques[2] = new Point((int)(p3.rendreX()+points_cliques[0].distance(p1)),p3.rendreY());
					
				}
				
			}
			
			points_cliques[1] = p1;
			points_cliques[3] = p3;
			
			
			//Modification des points a fabriquer
			figure_en_cours_de_fabrication.modifierPoints(points_cliques);
			src.ajoute(figure_en_cours_de_fabrication);
			
			new_fig = new Parallelogramme();
			new_fig.changerCouleur(Fenetre.COULEUR_COURANTE);
			src.supprimerAuditeurs();
			src.construit(new_fig);
			
		}
		
		
		//Si la figure a fabriquer est un cercle
		if((figure_en_cours_de_fabrication instanceof Cercle) && (nb_points_cliques == 2)) {
			
			//Si on initialise le rayon vers le bas
			if(points_cliques[1].rendreY() > points_cliques[0].rendreY()) {
				points_cliques[1] = new Point(points_cliques[0].rendreX(),(int)(points_cliques[1].rendreY()-2*points_cliques[1].distance(points_cliques[0])));
			} else {
				points_cliques[1] = new Point(points_cliques[0].rendreX(),points_cliques[1].rendreY());
			}
			
			
			//Modification des points de la figure
			figure_en_cours_de_fabrication.modifierPoints(points_cliques);
			src.ajoute(figure_en_cours_de_fabrication);
			
			new_fig = new Cercle();
			new_fig.changerCouleur(Fenetre.COULEUR_COURANTE);
			src.supprimerAuditeurs();
			src.construit(new_fig);
			
		}
		
		
		//Si la figure a fabriquer est une ellipse
		if((figure_en_cours_de_fabrication instanceof Ellipse) && (nb_points_cliques == 3)) {
			
			
			//Si on commence a construire l'ellipse sur le point droit ou le point gauche
			if(((points_cliques[1].rendreX() > points_cliques[0].rendreX()) && (points_cliques[1].rendreX() < points_cliques[2].rendreX()))
				|| ((points_cliques[1].rendreX() < points_cliques[0].rendreX()) && (points_cliques[1].rendreX() > points_cliques[2].rendreX()))) {
				
				System.out.println("OK1");
				
				Point tmp = points_cliques[0];
				Point tmp2 = new Point(points_cliques[1].rendreX(),points_cliques[0].rendreY());
				
				//Si on se dirige d'abord vers le haut
				if(points_cliques[1].rendreY() < points_cliques[0].rendreY()) {
					
					//Si on commence sur le point gauche
					if(points_cliques[2].rendreX() > points_cliques[0].rendreX()) {
						
						points_cliques[0] = points_cliques[1];
						points_cliques[1] = new Point((int)(tmp2.rendreX()+tmp2.distance(tmp)),tmp.rendreY());
						points_cliques[2] = new Point(tmp2.rendreX(),(int)(tmp2.rendreY()+tmp2.distance(points_cliques[0])));
					
					//Si on commence sur le point droit
					} else {
						
						points_cliques[0] = points_cliques[1];
						points_cliques[1] = tmp;
						points_cliques[2] = new Point(tmp2.rendreX(),(int)(tmp2.rendreY()+tmp2.distance(points_cliques[0])));
						System.out.println("OK");
						
					}
				
				//Si on se dirige d'abord vers le bas	
				} else {
					
					//Si on commence sur le point gauche
					if(points_cliques[2].rendreX() > points_cliques[0].rendreX()) {
					
						points_cliques[2] = points_cliques[1];
						points_cliques[1] = new Point((int)(tmp2.rendreX()+tmp2.distance(tmp)),tmp.rendreY());
						points_cliques[0] = new Point(tmp2.rendreX(),(int)(tmp2.rendreY()-tmp2.distance(points_cliques[2])));
					
					//Si on commence sur le point droit
					} else {
						
						points_cliques[2] = points_cliques[1];
						points_cliques[1] = new Point((int)(tmp2.rendreX()+tmp2.distance(tmp)),tmp2.rendreY());
						points_cliques[0] = new Point(tmp2.rendreX(),(int)(tmp2.rendreY()-tmp2.distance(points_cliques[2])));
						
					}
				}

			//Si on commence sur le point haut ou le point bas	
			} else {
				
				points_cliques[2] = new Point(points_cliques[0].rendreX(),points_cliques[2].rendreY());
				
				if(points_cliques[2].rendreY() > points_cliques[1].rendreY()) {
					
					points_cliques[1] = new Point(points_cliques[1].rendreX(),(int)(points_cliques[2].rendreY()-(points_cliques[0].distance(points_cliques[2])/2)));
				
				} else {
					
					points_cliques[1] = new Point(points_cliques[1].rendreX(),(int)(points_cliques[2].rendreY()+(points_cliques[0].distance(points_cliques[2])/2)));
				
				}
					
				Point tmp = new Point(points_cliques[0].rendreX(),points_cliques[1].rendreY());
				
				//Si on commence du point bas
				if(points_cliques[0].rendreY() > points_cliques[2].rendreY()) {
					Point inversion = points_cliques[0];
					points_cliques[0] = points_cliques[2];
					points_cliques[2] = inversion;
				}
				
				//Si l'abscisse du point haut est superieure a l'abscisse du point droit
				if(points_cliques[0].rendreX() > points_cliques[1].rendreX()) {
					points_cliques[1] = new Point((int)(points_cliques[1].rendreX()+tmp.distance(points_cliques[1])*2),points_cliques[1].rendreY());
				}
				
			}
			
			Point tmp3 = new Point(points_cliques[0].rendreX(),points_cliques[1].rendreY());
			points_cliques[3] = new Point((int)(tmp3.rendreX()-tmp3.distance(points_cliques[1])),points_cliques[1].rendreY());
			
			
			//Modification des points de la figure
			figure_en_cours_de_fabrication.modifierPoints(points_cliques);
			src.ajoute(figure_en_cours_de_fabrication);
			
			new_fig = new Ellipse();
			new_fig.changerCouleur(Fenetre.COULEUR_COURANTE);
			src.supprimerAuditeurs();
			src.construit(new_fig);
			
		}
		
		src.repaint();

	}

	public void mouseEntered(MouseEvent e) {
		
	}

	public void mouseExited(MouseEvent e) {
		
	}

}
