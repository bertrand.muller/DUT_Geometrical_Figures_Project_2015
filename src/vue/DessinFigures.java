package vue;

import modele.FigureColoree;
import modele.Point;
import modele.Segment;

import java.awt.Graphics;
import java.awt.MenuContainer;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.EventListener;

import javax.accessibility.Accessible;
import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.KeyStroke;
import javax.swing.ProgressMonitorInputStream;
import javax.swing.SwingWorker;

import application.Fenetre;
import controleur.FabricantFigures;


/**
 * Classe modelisant un objet comportant plusieurs figures 
 * colorees visualisables dont une seule est selectionnee
 */
public class DessinFigures extends JPanel implements ImageObserver, MenuContainer, Serializable, Accessible {

	
	/**
	 * Liste de figures colorees
	 */
	private ArrayList<FigureColoree> lfg;
	
	
	/**
	 * Listener pour les manipulations et transformations de figures via la souris
	 */
	private ManipulateurFormes mf;
	
	
	/**
	 * Nombre effectif de figures apparaissant dans ce dessin
	 */
	private int nbf;
	
	
	/**
	 * Indice de la figure actuellement selectionnee
	 */
	private int sel;
	
	//Attributs pour le trace
	
	/**
	 * Tableau de segments
	 */
	private Segment[] tab_seg;
	
	
	/**
	 * Indice du segment
	 */
	private int indice_seg;
	
	
	/**
	 * Booleen indiquant si on est en train de tracer
	 */
	private boolean trace_seg;
	
	
	/**
	 * Booleen indiquant si le trace a ete initialise
	 */
	private boolean init_trace;
	
	
	/**
	 * Constructeur d'un dessin de figures
	 */
	public DessinFigures() {
		lfg = new ArrayList<FigureColoree>();
		mf = null;
		nbf = 0;
		sel = -1;
		tab_seg = new Segment[1000000];
		indice_seg = 0;
		trace_seg = false;
		init_trace = false;
		initialiserKeyBindings();
	}
	

	/**
	 * Methode redessinant toutes les figures du dessin
	 * 
	 * @param g
	 * 			Environnement graphique
	 */
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
	
		int taille = lfg.size();
		
		for(int i=0; i < taille; i++) {
			lfg.get(i).affiche(g);
		}
		
		
		int i = 0;

		while (i != this.indice_seg) {

			g.setColor(tab_seg[i].getCouleur());

			Point dep = tab_seg[i].getDep();
			Point arr = tab_seg[i].getArr();

			g.drawLine(dep.rendreX(), dep.rendreY(), arr.rendreX(), arr.rendreY());

			i++;
		}
		
		
	}
	
	
	/**
	 * Methode activant les manipulations des formes geometriques a la souris
	 * 
	 * @param fc
	 * 			Figure a ajouter au dessin
	 */
	public void ajoute(FigureColoree fc) {
		lfg.add(fc);
	}
	
	
	/**
	 * Methode permettant d'initier le mecanisme evenementiel 
	 * de fabrication des figures a la souris (ajout du listener)
	 * 
	 * @param fc
	 * 			Figure a construire point par point a la souris
	 */
	public void construit(FigureColoree fc) {
		
		
		FabricantFigures ff = new FabricantFigures(fc);
		
		this.addMouseListener(ff);

	}
	
	
	/**
	 * Methode activant les manipulations des formes geometriques a la souris
	 */
	public void activeManipulationsSouris() {
		
		mf = new ManipulateurFormes();
		addMouseListener(mf);
		addMouseMotionListener(mf);			
		
	}
	
	
	/**
	 * Methode desactivant les manipulations des formes geometriques a la souris
	 */
	public void desactiveManipulationsSouris() {
		
		
		supprimerAuditeurs();
		
		if(sel != -1) {
			lfg.get(sel).deSelectionne();
			sel = -1;
		}
		
		repaint();
	}
	
	
	/**
	 * Methode retournant la figure actuellement selectionnee
	 * 
	 * @return
	 * 			Figure selectionnee
	 */
	public FigureColoree figureSelection() {
		return lfg.get(sel);
	}
	
	
	/**
	 * Methode retournant le nombre de figures apparaissant dans ce dessin
	 * 
	 * @return
	 * 			Nombre de figures sur le dessin
	 */
	public int nbFigures() {
		return lfg.size();
	}
	
	
	/**
	 * Methode selectionnant la prochaine figure dans la tableau des figures
	 */
	public void selectionProchaineFigure() {
		
		if(sel == lfg.size()-1) {
			sel = 0;
		} else {
			sel++;
		}
	}
	
	
	/**
	 * Methode supprimant l'ensemble des auditeurs lies a ce dessin
	 */
	public void supprimerAuditeurs() {
		
		
		//On recupere l'ensemble des listeners
		MouseListener[] tab_list = this.getMouseListeners();
		
		for(int i = 0; i< tab_list.length; i++){
			
			this.removeMouseListener(tab_list[i]);
			
		}
		
		
		removeMouseMotionListener(mf);
		trace_seg = false;
		
	}
	
	
	/**
	 * Methode permettant d'initier le mecanisme evenementiel de trace quelconque a la souris
	 */
	public void trace() {
		
		trace_seg = true;		
		mf = new ManipulateurFormes();
		addMouseMotionListener(mf);
		addMouseListener(mf);
		init_trace = true;
		
	}
	
	
	/**
	 * Methode permettant de supprimer une figure a indice donne
	 * 
	 * @param indice
	 * 			Indice de la figure dans la liste
	 */
	public void supprimerFigure(int indice){
		
		lfg.remove(indice);
		
	}
	
	
	/**
	 * Methode permettant de supprimer l'ensemble des figures du dessin
	 */
	public void toutEffacer(){
		
		supprimerAuditeurs();
		lfg = new ArrayList<FigureColoree>();
		mf = null;
		nbf = 0;
		sel = -1;
		tab_seg = new Segment[1000000];
		indice_seg = 0;
		repaint();

	}
	
	
	/**
	 * Methode permettant d'initliasier les KeyBindings
	 */
	private void initialiserKeyBindings() {
		
		this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "pressed");
		this.getActionMap().put("pressed", new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JComboBox<String> figure_choix = (JComboBox<String>) getAccessibleContext().getAccessibleChild(0).getAccessibleContext().getAccessibleChild(3);
				JComboBox<String> couleur_choix = (JComboBox<String>) getAccessibleContext().getAccessibleChild(0).getAccessibleContext().getAccessibleChild(4);
				
				if(sel != -1){
					supprimerFigure(sel);
					sel = -1;
					figure_choix.setEnabled(false);
					couleur_choix.setEnabled(false);
					repaint();
					
				}
				
				
			}
		});
		
	}
	
	
	/**
	 * Methode permettant de charger un fichier
	 * 
	 * @param fich
	 * 			Fichier a charger
	 */
	public void charger(File fich) {

		try {
	
			FileInputStream stream = new FileInputStream(fich);
			
			
			 final ProgressMonitorInputStream progress = new ProgressMonitorInputStream(this, "Chargement de "+ fich.getName(), stream);
			 progress.getProgressMonitor().setMillisToDecideToPopup(5);

			 SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>()
			         {
			            protected Void doInBackground() throws Exception
			            {
			            	
			            	
			            	ArrayList<FigureColoree> save_lfg = lfg;
			            	int save_nbf = nbf;
			            	int save_sel = sel;
			            	Segment[] save_segs = tab_seg;
			            	int save_indSeg = indice_seg;
			            	boolean save_trace = trace_seg;
			            	boolean save_initTrace = init_trace;
			            	
			            	
			            	
			            	
			            	
			            	ObjectInputStream ois = new ObjectInputStream(progress);
			            	Boolean charg = false;
			            	
			            	try{
			      			              
			            	lfg = (ArrayList<FigureColoree>) ois.readObject();
			    			nbf = (int) ois.readObject();
			    			sel = (int) ois.readObject();
			    			tab_seg = (Segment[]) ois.readObject();
			    			indice_seg = (int) ois.readObject();
			    			trace_seg = (boolean) ois.readObject();
			    			init_trace = (boolean) ois.readObject();
			    			desactiveManipulationsSouris();
			    			charg = true;
			    			
			            	} catch(IOException stop){
			            		
			            		lfg = save_lfg;
			            		nbf = save_nbf;
			            		sel = save_indSeg;
			            		tab_seg = save_segs;
			            		indice_seg = save_indSeg;
			            		trace_seg = save_trace;
			            		init_trace = save_initTrace;
			            		
			            	}
			            	ois.close();
			            	progress.close();
			            	repaint();
			            	
			            	if(charg){
			            		
			            		JOptionPane.showMessageDialog(null, "Chargement effectue ! ");
			            		
			            	}else{
			            		
			            		JOptionPane.showMessageDialog(null, "Le chargement a echoue ou a ete annule","Erreur", JOptionPane.ERROR_MESSAGE);
			            	}
			            
			               return null;
			            }
			         };
			      worker.execute();
			   
		
			this.repaint();
			
			
			JRadioButton maniuplation = (JRadioButton) getAccessibleContext().getAccessibleChild(0).getAccessibleContext().getAccessibleChild(2);
			
			if(maniuplation.isSelected()){
				
				activeManipulationsSouris();
				
			}
			
			System.out.println("Chargement effectu� !");

		} catch (IOException e) {

			e.printStackTrace();

		} 

	}

	
	/**
	 * Methode permettant d'enregistrer un fichier
	 * 
	 * @param fich
	 * 			Fichier a enregistrer
	 */
	public void enregistrer(File fich) {

		try {

			ObjectOutputStream dos = new ObjectOutputStream(new FileOutputStream(fich));			
			dos.writeObject(this.lfg);
			dos.writeObject(this.nbf);
			dos.writeObject(this.sel);
			dos.writeObject(this.tab_seg);
			dos.writeObject(this.indice_seg);
			dos.writeObject(this.trace_seg);
			dos.writeObject(this.init_trace);
			System.out.println("Enregistrement effectue !");
			dos.close();
			
		}catch(FileNotFoundException ff){
			
			System.out.println("Fichier non trouve !");
			
		}catch(IOException oe){
			
			System.out.println("Une erreur d'entree/sortie est survenue !");
			oe.printStackTrace();
		}

	}
	
	
	/**
	 * Classe interne pour le deplacement et la transformation des figures geometriques
	 */
	private class ManipulateurFormes implements Serializable, MouseListener, MouseMotionListener, EventListener {
		
		/**
		 * Attribut indiquant l'indice du point proche d'un carre de selection
		 */
		private int indice;
		
		
		/**
		 * Abscisse d'un clic de souris
		 */
		private int last_x;
		
		
		/**
		 * Ordonnee d'un clic de souris
		 */
		private int last_y;
		
		
		/**
		 * Attribut indiquant si un deplacement est en cours
		 */
		private boolean trans;
		
		
		/**
		 * Constructeur d'un manipulateur de formes
		 */
		private ManipulateurFormes() {
			indice = 0;
			last_x = 0;
			last_y = 0;
			trans = false;
		}


		/**
		 * Methode deplacant ou transformant la figure geometrique selectionnee
		 * 
		 * @param e
		 * 			Action de souris
		 */
		public void mouseDragged(MouseEvent e) {
			
			//Recuperation de la position de la souris
			int x_tmp = e.getX();
			int y_tmp = e.getY();
			
			
			//On teste si l'utilisateur souhaite realiser un trace a main levee
			if(trace_seg){
					
				
				//On teste si le trace a main levee a ete initialise
				if(init_trace){
					
					last_x = e.getX();
					last_y = e.getY();
					init_trace = false;
					
				}
				
				
				//On empeche le trace au niveau du panneau de choix
				if(e.getY() > 105) {
					tab_seg[indice_seg] = new Segment(new Point(last_x,last_y),new Point(x_tmp,y_tmp), Fenetre.COULEUR_COURANTE);
					indice_seg++;
					repaint();
				}
				
				last_x = e.getX();
				last_y = e.getY();
				
			}
			
					
			//On teste si il n'y a aucune figure sleectionnee
			if(sel != -1){		

				FigureColoree fig = lfg.get(sel);
				
				
				//Si on est sur la figure mais pas sur un carre de selection
				if (fig.estDedans(x_tmp, y_tmp) && (fig.carreDeSelection(x_tmp, y_tmp)== -1)) {

					Point p_prec = new Point(last_x, last_y);
					Point p_cour = new Point(e.getX(), e.getY());
					Point tmp = new Point(last_x, e.getY());

					int transX = (int) p_cour.distance(tmp);
					int transY = (int) tmp.distance(p_prec);
					
					if(last_x > e.getX()){
						
						transX = -transX;
						
					}
					
					if(last_y > e.getY()){
						
						transY = -transY;
						
					}
					
					
					lfg.get(sel).translation(transX, transY);
					
				}
				
				
				//Si au moins un carre de selection est utilise
				if(fig.carreDeSelection(x_tmp, y_tmp) != -1){
					
					
					Point p_cour = new Point(x_tmp,y_tmp);
					Point p_prec = new Point(last_x, last_y);					
					Point tmp = new Point(last_x, e.getY());

					
					int transX = (int) p_cour.distance(tmp);
					int transY = (int) tmp.distance(p_prec);
					
					
					int indice_carre = fig.carreDeSelection(x_tmp, y_tmp);

					
					if(last_x > e.getX()){
						
						transX = -transX;
						
					}
					
					if(last_y > e.getY()){
						
						transY = -transY;
						
					}
					
					
					fig.transformation(transX, transY, indice_carre);
					
				}
				
				last_x = e.getX();
				last_y = e.getY();
				
				repaint();
						
			}
		}


		public void mouseMoved(MouseEvent e) {
			
		}


		public void mouseClicked(MouseEvent e) {
			
		
		}


		public void mousePressed(MouseEvent e) {
							
			last_x = e.getX();
			last_y = e.getY();
			
			boolean uneFigureSelec = false;
			
			
			//On parcourt l'ensemble des figures
			for(int i=0; i < lfg.size(); i++) {
				
				boolean dedans = lfg.get(i).estDedans(last_x, last_y);
				
				if(dedans && !trace_seg){
					
					lfg.get(i).selectionne();
					
					
					if(sel != -1 && sel != i){
						
						lfg.get(sel).deSelectionne();
						
						
					}	
					
					sel = i;
					uneFigureSelec = true;
			
					
				} else{
					
					lfg.get(i).deSelectionne();
					
				}
				
				repaint();
			}
			
			JComboBox<String> choix_coul = (JComboBox<String>) getAccessibleContext().getAccessibleChild(0).getAccessibleContext().getAccessibleChild(4);
			
			if(!uneFigureSelec && !trace_seg) {
				
				choix_coul.setEnabled(false);
				sel = -1;
				
			}else{
				
				choix_coul.setEnabled(true);
				
			}
			
		}


		/**
		 * Methode appelee lorsque l'utilisateur relache un bouton de souris
		 * 
		 * @param e
		 * 			Action de souris
		 */
		public void mouseReleased(MouseEvent e) {
			
			
			//On teste que l'utilisateur utilise la fonction trace a main levee
			if(trace_seg){
				
				init_trace = true;
			}
			
		}


		public void mouseEntered(MouseEvent e) {
			
		}


		public void mouseExited(MouseEvent e) {
			
		}
		
	}
	
}
