package vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import application.Fenetre;


/**
 * 
 * @author Bertrand
 *
 */
public class MenuGraphique extends JMenuBar {
	
	
		/**
		 * Fenetre
		 */
		private Fenetre fen;
	
		
		/**
		 * Constructeur d'un menu graphique
		 * 
		 * @param src
		 * 			Fenetre ou se trouve ce menu
		 */
		public MenuGraphique(Fenetre src){
			
			this.fen = src;
			ActionListener action_menu = new gestionMenu();
			
			/*		MENU FICHIER   	*/
			
			JMenu fic = new JMenu("Fichier");
			this.add(fic);
			
			JMenuItem cha = new JMenuItem("Ouvrir");		
			cha.addActionListener(action_menu);			
			fic.add(cha);
			

			JMenuItem enr = new JMenuItem("Enregistrer");			
			enr.addActionListener(action_menu);
			fic.add(enr);
			
			/*		MENU EDITION		*/
			
			JMenu edit = new JMenu("Edition");
			this.add(edit);
			
			
			JMenuItem eff = new JMenuItem("Tout Effacer");
			eff.addActionListener(action_menu);
			edit.add(eff);
			
			
			/*		MENU AIDE		*/
			
			JMenu aide = new JMenu("?");
			this.add(aide);
			
			JMenuItem tuto = new JMenuItem("Aide");
			tuto.addActionListener(action_menu);
			aide.add(tuto);
			
			JMenuItem info = new JMenuItem("A propos");
			info.addActionListener(action_menu);
			aide.add(info);
		
			
		}
		
		
		/**
		 * Classe interne permettant de gerer les actions de l'utilisateur sur le menu
		 *
		 */
		private class gestionMenu implements ActionListener {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JMenuItem srcEvent = (JMenuItem) e.getSource();
				
				switch(srcEvent.getText()){
				
				case "Ouvrir":
					
					JFileChooser jf_o = new JFileChooser();
					int res = jf_o.showOpenDialog(fen);
					if(res == JFileChooser.APPROVE_OPTION){
						
						System.out.println(jf_o.getSelectedFile().getName());
						
						String[] nom_fich = jf_o.getSelectedFile().getName().split("\\.");
						
						if(nom_fich[nom_fich.length -1].equals("dessin")){
							
							
							fen.lancer_chargement(jf_o.getSelectedFile());						
							
						}else{
							
							
							JOptionPane.showMessageDialog(null, "Le type du fichier est incorrect (extension .dessin)","Erreur", JOptionPane.ERROR_MESSAGE);
							
						}
						
						
					}
					break;
					
				case "Enregistrer":
					
					JFileChooser jf_e = new JFileChooser();
					res = jf_e.showSaveDialog(fen);
					if(res == JFileChooser.APPROVE_OPTION){
						
						File save = jf_e.getSelectedFile();
						
						if(!save.getAbsolutePath().endsWith(".dessin")){
							
							save = new File(jf_e.getSelectedFile() + ".dessin");
						}
	
						fen.lancer_enregistrement(save);
					}
					break;
				
				case "Tout Effacer":
					fen.effacerDessin();
					break;
					
				case "Aide":
					
					
					try {
						
						BufferedReader br = new BufferedReader(new FileReader("aide.html"));
						
						String ligne = "";
						String aide = "";
						
						while ((ligne = br.readLine()) != null) {
						   aide += ligne;
						}

						br.close();
						
						JLabel jl = new JLabel(aide);
						jl.setForeground(Color.BLACK);
						jl.setBackground(Color.WHITE);
						JScrollPane scrollPane = new JScrollPane(jl); 
						scrollPane.setPreferredSize(new Dimension(767,500));
						JOptionPane.showMessageDialog(null, scrollPane, "Aide", JOptionPane.INFORMATION_MESSAGE);
						
					} catch (IOException e1) {
						e1.printStackTrace();
					}					
					
					break;
					
				case "A propos":
					
					JOptionPane.showMessageDialog(null, "V5.0 \n " +"Programme realise dans le cadre du DUT Informatique a l'IUT"
																+ " Nancy-Charlemagne" +" \nAuteurs : " + "MULLER Bertrand "
																+ "et LEDOUX Florian", "A propos", JOptionPane.INFORMATION_MESSAGE);
					break;
				}
	
			}
			
		}
		
}
