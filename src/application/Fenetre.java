package application;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.MenuContainer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.Serializable;

import javax.accessibility.Accessible;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.RootPaneContainer;
import javax.swing.WindowConstants;

import vue.DessinFigures;
import vue.MenuGraphique;
import modele.Carre;
import modele.Cercle;
import modele.Ellipse;
import modele.FigureColoree;
import modele.Losange;
import modele.Parallelogramme;
import modele.Rectangle;
import modele.Triangle;
import modele.FigureQuelconque;


/**
 * Classe definissant l'interface utilisateur et la methode main
 */
public class Fenetre extends JFrame implements ImageObserver, MenuContainer, Serializable, Accessible, RootPaneContainer, WindowConstants {
	
	
	/**
	 * JPanel contenant la fenetre principale
	 */
	private DessinFigures dessin;
	
	
	/**
	 * Constante qui contient la couleur courante
	 */
	public static Color COULEUR_COURANTE = Color.RED;
	
	
	/**
	 * Constructeur d'une Fenetre
	 * 
	 * @param s
	 * 			Titre de la fenetre
	 * 
	 * @param h
	 * 			Largeur de la fenetre
	 * 
	 * @param w
	 *			Hauteur de la fenetre
	 */
	public Fenetre(String s, int h, int w){
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle(s);
		
		//Creation du JPanel principal
		dessin = new DessinFigures();
		
		//Creation du panneau de choix
		PanneauChoix pc = new PanneauChoix();
		pc.setBorder(BorderFactory.createTitledBorder("Actions sur les figures"));
		dessin.add(pc);		
		dessin.setPreferredSize(new Dimension(h,w));
		dessin.setFocusable(true);	
		this.setContentPane(dessin);
		this.pack();
		
		//Ajout du menu
		this.setJMenuBar(new MenuGraphique(this));
		
		this.setResizable(true); 
		this.setVisible(true);
		
	}
	
	
	/**
	 * Programme principal
	 * 
	 * @param args
	 * 			Arguments eventuels lors de l'execution
	 */
	public static void main(String[] args){
		
		Fenetre f = new Fenetre("Figures Geometriques",800,800);
		
	}

	
	/**
	 * Methode de lancement de l'enregistrement d'un fichier
	 * 
	 * @param fich
	 * 			Fichier a enregistrer
	 */
	public void lancer_enregistrement(File fich){

		//Enregistrement du fichier
		this.dessin.enregistrer(fich);

		//Affichage d'une fenetre de dialogue
		JOptionPane.showMessageDialog(null, "Enregistrement effectu� ! ");
		
	}
	
	
	/**
	 * Methode de lancement du chargement
	 * 
	 * @param fich
	 * 			Fichier a charger
	 */
	public void lancer_chargement(File fich){

		//Chargement du fichier
		this.dessin.charger(fich);

	}
	
	
	/**
	 * Methode permettant d'effacer l'ensemble des dessins realises
	 */
	public void effacerDessin(){
		
		this.dessin.toutEffacer();
		
	}
	
	
	/**
	 * Classe interne pour la construction de l'interface utilisateur
	 */
	private class PanneauChoix extends JPanel implements Serializable, ActionListener{
		
		
		/**
		 * Figure coloree a construire
		 */
		private FigureColoree fc;
		
		
		/**
		 * Constructeur d'un PanneauChoix
		 * (interface utilisateur)
		 */
		public PanneauChoix(){
			
			//Initialisation du gestionnaire de mise en page
			setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.weightx = 1;
			gbc.weighty = 1;
			gbc.gridwidth = 1;
			gbc.gridheight = 1;
			gbc.gridy = 0;
			gbc.fill = GridBagConstraints.CENTER;
			gbc.insets = new Insets(5, 15, 5, 15);
					
			
			//Creation des boutons radios (inclus dans un groupe)
			ButtonGroup group = new ButtonGroup();
						
			JRadioButton[] boutons_r = {new JRadioButton("Nouvelle Figure"),
										new JRadioButton("Trace a main levee"),
										new JRadioButton("Manipulations")};
			
			//Placement des boutons dans la fenetre et ajout des listeners associes
			for(int i = 0; i< boutons_r.length; i++){
				
				gbc.gridx = i;
				group.add(boutons_r[i]);
				this.add(boutons_r[i],gbc);
				boutons_r[i].addActionListener(this);
				
			}
			
			//Bouton 'Nouvelle Figure' coche par defaut 
			boutons_r[0].setSelected(true);
			
			
			//Creation du menu de selection des figures
			JComboBox<String>  choix_fig = new JComboBox<String>();
			choix_fig.setName("figures");
			
			
			String[] ens_fig = {"Rectangle", "Triangle", "Carre", "Losange", "Figure Quelconque", "Parallelogramme", "Cercle", "Ellipse"};
			
			
			//Ajout des items dans le menu de selection des figures
			for(int i = 0; i< ens_fig.length ; i++){
				
				choix_fig.addItem(ens_fig[i]);
				
			}
			
			gbc.gridx = 0;
			gbc.gridwidth = 2;
			gbc.gridy = 1;
			
			
			//Ajout du listener sur le menu de selection des figures
			choix_fig.addActionListener(this);
			
			this.add(choix_fig, gbc);
		
			
			//Creation du menu de selection des couleurs
			JComboBox<String>  choix_coul = new JComboBox<String>();
			choix_coul.setName("couleurs");
			
			
			String[] ens_coul = {"Rouge", "Vert", "Bleu", "Jaune", "Noir", "Violet", "Rose", "Orange"};
			
			
			//Ajout des items dans le menu de selection des couleurs
			for(int i = 0; i< ens_coul.length ; i++){
				
				choix_coul.addItem(ens_coul[i]);
				
			}
			
			gbc.gridx = 1;
			gbc.gridwidth = 2;
			gbc.gridy = 1;
			
			
			//Ajout du listener sur le menu de selection des couleurs
			choix_coul.addActionListener(this);
			
			this.add(choix_coul,gbc);
			
			
			//Creation par defaut d'un rectangle
			this.creeFigure(0);
			
		}
		
		
		/**
		 * Methode implementant la creation d'une figure coloree
		 * 
		 * @param index
		 * 			Indice representant la figure a construire
		 * 
		 * @return
		 * 			Figure construite
		 */
		private FigureColoree creeFigure(int index){
			
			/* Ensemble des indices associes a une figure :
					Indice 0 --> Rectangle
					Indice 1 --> Triangle
					Indice 2 --> Carre
					Indice 3 --> Losange
					Indice 4 --> Figure Quelconque
					Indice 5 --> Parallelogramme
					Indice 6 --> Cercle
					Indice 7 --> Ellipse*/
			
			FigureColoree f = null;
		
			
			//Switch permettant de creer la figure a contruire selon l'indice
			switch(index) {
			case 0:
				f = new Rectangle();
				break;
				
			case 1:
				f = new Triangle();
				break;
				
			case 2:
				f = new Carre();
				break;
				
			case 3:
				f = new Losange();
				break;
				
			case 4:
				
				//Affichage d'une fenetre de dialogue permettant a l'utilisateur de choisir un nombre de points
				String s = (String)JOptionPane.showInputDialog(null, "Entrez le nombre de points de votre figure :", "Nombre de points", JOptionPane.PLAIN_MESSAGE);
				
				try {
					
					int nbPoints = Integer.parseInt(s);
					f = new FigureQuelconque(nbPoints);
					
					JOptionPane.showConfirmDialog(null, "Vous pouvez tracer votre figure en cliquant " + nbPoints + " fois.", 
							"Information", JOptionPane.DEFAULT_OPTION);
					
				} catch(NumberFormatException nfe) {
					
					JOptionPane.showConfirmDialog(null, "Nombre invalide ou operation abandonnee ! Le nombre de points a ete initialise par defaut a 5. \n\n Appuyez sur OK et selectionnez a nouveau 'Figure Quelconque' si vous souhaitez changer.", 
							"Erreur", JOptionPane.CLOSED_OPTION);
					
					//Si erreur, initialisation de la figure avec 5 points 
					f = new FigureQuelconque(5);
					
				}
				
				break;
				
			case 5:
				
				f = new Parallelogramme();
				break;
				
			case 6:
				
				f = new Cercle();
				break;
				
			case 7:
				
				f = new Ellipse();
				break;
				
			}
			
			//Couleur de la figure correspondant a la couleur courante
			f.changerCouleur(COULEUR_COURANTE);
			
			fc = f;
			
			//Construction de la figure
			dessin.construit(f);
			
			return f;
			
		}
		
		
		/**
		 * Methode determinant la couleur a utiliser
		 * 
		 * @param index
		 * 			Indice representant la couleur a utiliser
		 * 
		 * @return
		 * 			Couleur a utiliser
		 */
		private Color determineCouleur(int index){
			
			/* Ensemble des indices associes a une figure :
					Indice 0 --> Rouge
					Indice 1 --> Vert
					Indice 2 --> Bleu
					Indice 3 --> Jaune
					Indice 4 --> Noir
					Indice 5 --> Violet
					Indice 6 --> Rose
					Indice 7 --> Orange*/
			
			Color c = null;
			
			
			//Switch permettant de creer la couleur selon l'indice
			switch(index) {
			
			case 0:
				c = Color.RED;
				break;
				
			case 1:
				c = Color.GREEN;
				break;
				
			case 2:
				c = Color.BLUE;
				break;
				
			case 3:
				c = Color.YELLOW;
				break;
				
			case 4:
				c = Color.BLACK;
				break;
				
			case 5:
				c = new Color(145, 18, 141);
				break;
				
			case 6:
				c = Color.MAGENTA;
				break;
				
			case 7:
				c = Color.ORANGE;
				break;
			
			}
			
			return c;
			
		}


		@Override
		/**
		 * Methode appelee quand une action se produit sur les boutons ou les listes deroulantes
		 * 
		 * @param ae
		 * 			Action produite
		 */
		public void actionPerformed(ActionEvent ae) {

			
			//Recuperation de l'objet source
			Object obj = ae.getSource();

			
			if (obj instanceof JRadioButton) {
				
				dessin.supprimerAuditeurs();

				JRadioButton jrb = (JRadioButton) obj;
				
				//Recuperation de la liste de figures
				JComboBox<String> figure_choix = (JComboBox<String>) getAccessibleContext().getAccessibleChild(3);
				
				//Recuperation de la liste de couleurs
				JComboBox<String> couleur_choix = (JComboBox<String>) getAccessibleContext().getAccessibleChild(4);


				//Test pour savoir quelle action est demandee par l'utilisateur
				switch (jrb.getText()) {

				case "Nouvelle Figure":
					
					couleur_choix.setEnabled(true);
					figure_choix.setEnabled(true);
					dessin.desactiveManipulationsSouris();
						
					
					int indice_fig = 0;
					
					
					//Test pour savoir quelle figure l'utilisateur a selectonniee
					switch(figure_choix.getSelectedItem().toString()){
					
					case "Rectangle":
						
						indice_fig = 0;
						break;
					case "Triangle":
						
						indice_fig = 1;
						break;
						
					case "Carre":
						
						indice_fig = 2;
						break;
						
					case "Losange":
						
						indice_fig = 3;
						break;
					
					case "Figure Quelconque":
						
						indice_fig = 4;
						break;
						
					case "Parallelogramme":
						
						indice_fig = 5;
						break;
					
					case "Cercle":
						
						indice_fig = 6;
						break;
						
					case "Ellipse":
						
						indice_fig = 7;
						break;
						
					}
					
					
					//Creation de la figure selon l'indice de la figure selectionnee par l'utilisateur
					this.creeFigure(indice_fig);
					
					break;

				case "Trace a main levee":

					figure_choix.setEnabled(false);
					couleur_choix.setEnabled(true);
					dessin.desactiveManipulationsSouris();
					
					//Initialisation du trace a main levee
					dessin.trace();
					
					break;

				case "Manipulations":
					
					figure_choix.setEnabled(false);
					couleur_choix.setEnabled(false);
					
					dessin.activeManipulationsSouris();
					
					break;

				}

			}
			
			
			if( obj instanceof JComboBox<?>){
				
				
				//Recuperation de la JComboBox ayant ete activee
				JComboBox<String> choix = (JComboBox<String>) obj;
				
				
				//Test pour savoir s'il s'agit de la JComboBox des couleurs
				if(choix.getName().equals("couleurs")){
					
					Boolean selection = true;
					FigureColoree fig = null;
					
					
					try{
						
						fig = dessin.figureSelection();
					
					}catch(ArrayIndexOutOfBoundsException pasFigure){
						
						selection = false;
						
					}
					
					
					//Affectation de la couleur courante a la constante
					COULEUR_COURANTE = determineCouleur(choix.getSelectedIndex());
					
					if(selection){
						fig.changerCouleur(COULEUR_COURANTE);
						dessin.repaint();
					}
					
					JRadioButton jr = (JRadioButton) getAccessibleContext().getAccessibleChild(0);
					
					//On teste si le bouton 'Nouvelle Figure' est selectionne
					if(jr.isSelected()){
						
						dessin.supprimerAuditeurs();
						
						JComboBox<String> figure_choix = (JComboBox<String>) getAccessibleContext().getAccessibleChild(3);
						
						//Creation de la figure
						creeFigure(figure_choix.getSelectedIndex());
						
					}
					
				
				}
				
				
				//Test pour savoir s'il s'agit de la JComboBox des figures
				if(choix.getName().equals("figures")){
					
					JRadioButton crea_figure = (JRadioButton) getAccessibleContext().getAccessibleChild(0);
					
					
					//On teste si le bouton 'Nouvelle Figure' est selectionne
					if(crea_figure.isSelected()){
						
						dessin.desactiveManipulationsSouris();
						
						
						JComboBox<String> figure_choix = (JComboBox<String>) getAccessibleContext().getAccessibleChild(3);
						
						int indice_fig = 0;
						
						
						//Test pour savoir quelle figure est selectionnee
						switch(figure_choix.getSelectedItem().toString()){
						
						case "Rectangle":
							
							indice_fig = 0;
							break;
							
						case "Triangle":
							
							indice_fig = 1;
							break;
							
						case "Carre":
							
							indice_fig = 2;
							break;
							
						case "Losange":
							
							indice_fig = 3;
							break;
							
						case "Figure Quelconque":
							
							indice_fig = 4;
							break;
						
						case "Parallelogramme":
							
							indice_fig = 5;
							break;
							
						case "Cercle":
							
							indice_fig = 6;
							break;
							
						case "Ellipse":
							
							indice_fig = 7;
							break;
						
						}
						
						//Creation de la figure selon l'indice
						this.creeFigure(indice_fig);
						
					}
					
				}
				
			}
			
		}
		
	}
	
}
