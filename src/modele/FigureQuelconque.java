package modele;


/**
 * Classe modelisant une figure quelconque avec un certain nombre de points
 */
public class FigureQuelconque extends Polygone {

	
	/**
	 * Nombre de points dans la figure
	 */
	private int nbPoints;
	
	
	/**
	 * Constructeur d'une figure quelconque
	 * 
	 * @param n
	 * 			Nombre de points
	 */
	public FigureQuelconque(int n) {
		super();
		nbPoints = n;
	}

	
	@Override
	/**
	 * Methode retournant le nombre de points de memorisation d'une figure quelconque
	 * 
	 * @return
	 * 		Nombre de points
	 */
	public int nbPoints() {
		return nbPoints;
	}

	
	@Override
	/**
	 * Methode permettant d'effectuer une transformation des coordonnees
	 * des points de memorisation d'une figure quelconque
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 * 
	 * @param indice
	 * 			Indice dans tab_mem du point a modifier
	 */
	public void transformation(int dx, int dy, int indice) {
		
		tab_mem[indice].incrementerX(dx);
		
		
		//On teste que les points restent dans la zone de dessin
		if (tab_mem[indice].rendreY()+dy > 105) {
			tab_mem[indice].incrementerY(dy);
		}
		
		super.transformation(dx, dy, indice);
	}
	
}
