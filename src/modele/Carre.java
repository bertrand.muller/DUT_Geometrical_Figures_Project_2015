package modele;

import modele.Quadrilatere;


/**
 * Classe modelisant un carre
 */
public class Carre extends Quadrilatere {

	
	//Constructeur d'un carre
	public Carre() {
		super();
	}

	
	@Override
	/**
	 * Methode retournant le nombre de points necessaires 
	 * dont on a besoin pour la saisie d'un rectangle
	 * 
	 * @return
	 * 			Nombre de clics
	 * 
	 */
	public int nbClics() {

		return 1;

	}

	
	/**
	 * Methode permettant d'effectuer une transformation des coordonnees
	 * des points de memorisation d'un rectangle
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 * 
	 * @param indice
	 * 			Indice dans tab_mem du point a modifier
	 */
	public void transformation(int dx, int dy, int indice) {
		
		
		boolean incremY = false;
				
		
		tab_mem[indice].incrementerX(dx);
		
		//On teste que les points restent dans la zone de dessin
		if((tab_mem[indice].rendreY() > 105)){
		
			tab_mem[indice].incrementerY(dy);
			incremY = true;
		}
		
		
		//Test permettant de modifier les autres points en consequence, en fonction de l'indice
		switch (indice) {

		case 0:

			tab_mem[3].incrementerX(dx);
			tab_mem[3].incrementerY(-dx);
			tab_mem[2].incrementerY(-dx);

			if (incremY) {
				tab_mem[1].incrementerY(dy);
				tab_mem[1].incrementerX(-dy);
				tab_mem[2].incrementerX(-dy);
			}
			break;

		case 1:
			tab_mem[2].incrementerX(dx);
			tab_mem[2].incrementerY(dx);
			tab_mem[3].incrementerY(dx);

			if (incremY) {
				tab_mem[0].incrementerY(dy);
				tab_mem[0].incrementerX(dy);
				tab_mem[3].incrementerX(dy);
			}

			break;

		case 2:

			tab_mem[1].incrementerX(dx);
			tab_mem[1].incrementerY(-dx);
			tab_mem[0].incrementerY(-dx);

			if (incremY) {
				tab_mem[3].incrementerY(dy);
				tab_mem[3].incrementerX(-dy);
				tab_mem[0].incrementerX(-dy);
			}

			break;

		case 3:
			tab_mem[0].incrementerX(dx);
			tab_mem[0].incrementerY(dx);
			tab_mem[1].incrementerY(dx);

			if (incremY) {
				tab_mem[2].incrementerY(dy);
				tab_mem[2].incrementerX(dy);
				tab_mem[1].incrementerX(dy);
			}
			break;

		}
		
		//Appel a la super methode
		super.transformation(dx, dy, indice);
	
	}
	
}
