package modele;

import java.io.Serializable;


/**
 * Classe modelisant un Point
 */
public class Point implements Serializable {

	
	/**
	 * Coordonnees du point
	 */
	private int x, y;

	
	/**
	 * Constructeur d'un point
	 * 
	 * @param x
	 * 		Abscisse du point
	 * 
	 * @param y
	 * 		Ordonnee du point
	 */
	public Point(int x, int y) {

		this.x = x;
		this.y = y;

	}
	
	
	/**
	 * Methode calculant la distance entre deux points
	 * 
	 * @param p
	 * 			Deuxieme point
	 * 
	 * @return
	 * 			Distance entre les deux points
	 */
	public double distance(Point p){
		
		return Math.sqrt(Math.pow((p.rendreX() - x),2) + Math.pow((p.rendreY() - y),2));
		
	}
	

	/**
	 * Methode renvoyant l'abscisse du point
	 * 
	 * @return
	 * 			Abscisse du point
	 */
	public int rendreX() {

		return x;

	}

	
	/**
	 * Methode renvoyant l'ordonnee du point
	 * 
	 * @return
	 * 			Ordonnee du point
	 */
	public int rendreY() {

		return y;

	}
	

	/**
	 * Methode incrementant l'abscisse du point
	 * 
	 * @param val
	 * 			Valeur d'incrementation
	 */
	public void incrementerX(int val) {

			x += val;

	}


	/**
	 * MEthode incrementant l'ordonnee du point
	 * 
	 * @param val
	 * 			Valeur d'incrementation
	 */
	public void incrementerY(int val) {

			y += val;

	}
	
	
	/**Methode modifiant l'abscisse du point
	 * 
	 * @param new_x
	 * 			Nouvelle abscisse du point
	 */
	public void modifierX(int new_x){
		
		if(new_x > 0){
			
			x = new_x;
			
		}
		
	}
	
	
	/**
	 * Methode modifiant l'ordonnee du point
	 * 
	 * @param new_y
	 * 			Nouvelle ordonnee du point
	 */
	public void modifierY(int new_y){
		
		if(new_y > 0){
			
			x = new_y;
			
		}
		
	}
	
	
	/**
	 * Methode translatant le point
	 * 
	 * @param t_x
	 * 			Deplacement en abscisse
	 * 
	 * @param t_y
	 * 			Deplacement en ordonnee
	 */
	public void translation(int t_x, int t_y){
		
		x += t_x;
		y += t_y;
		
		
	}
	
}
