package modele;


/**
 * Classe modelisant un rectangle
 */
public class Rectangle extends Quadrilatere {
	
	
	/**
	 * Constructeur d'un rectangle
	 */
	public Rectangle() {
		super();
	}

	
	@Override
	/**
	 * Methode retournant le nombre de points dont a besoin,
	 * en general, pour la saisie d'un rectangle
	 * 
	 * @return
	 * 			Nombre de clics
	 */
	public int nbClics() {

		return 4;

	}

	
	@Override
	/**
	 * Methode permettant d'effectuer une transformation 
	 * des coordonnees des points de memorisation
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 * 
	 * @param indice
	 * 			Indice dans tab_mem du point a modifier
	 */
	public void transformation(int dx, int dy, int indice) {
		
		
		boolean incremY = false;
				
		
		tab_mem[indice].incrementerX(dx);
		
		
		//On parcourt l'ensemble des points de memorisation du rectangle
		if((tab_mem[indice].rendreY() > 105)){
		
			tab_mem[indice].incrementerY(dy);
			incremY = true;
		}
		
		
		//Test permettant de modifier les autres points en consequence, en fonction de l'indice 
		switch (indice) {

		case 0:

			tab_mem[3].incrementerX(dx);

			if (incremY) {
				tab_mem[1].incrementerY(dy);
			}
			break;

		case 1:
			
			tab_mem[2].incrementerX(dx);

			if (incremY) {
				tab_mem[0].incrementerY(dy);
			}

			break;

		case 2:

			tab_mem[1].incrementerX(dx);

			if (incremY) {
				tab_mem[3].incrementerY(dy);
			}

			break;

		case 3:
			
			tab_mem[0].incrementerX(dx);

			if (incremY) {
				tab_mem[2].incrementerY(dy);
			}
			break;

		}
		
		super.transformation(dx, dy, indice);
	
	}

}
