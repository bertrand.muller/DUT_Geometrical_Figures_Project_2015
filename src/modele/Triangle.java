package modele;

import java.awt.Polygon;


/**
 * Classe modelisant un triangle
 */
public class Triangle extends Polygone {

	
	/**
	 * Constructeur d'un triangle
	 */
	public Triangle() {
		super();
	}
	
	
	@Override
	/**
	 * Methode retournant le nombre de points de memorisation d'un triangle
	 * 
	 * @return
	 * 			Nombre de points
	 */
	public int nbPoints() {

		return 3;
	}
	
	
	@Override
	/**
	 * Methode permettant d'effectuer une transformation 
	 * des coordonnees des points de memorisation
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 * 
	 * @param indice
	 * 			Indice dans tab_mem du point a modifier
	 */
	public void transformation(int dx, int dy, int indice) {
		
		tab_mem[indice].incrementerX(dx);
		
		
		//On parcourt l'ensemble des points de memorisation du rectangle
		if(tab_mem[indice].rendreY()+dy > 105) {
			tab_mem[indice].incrementerY(dy);
		}
		
		super.transformation(dx, dy, indice);
		
	}

}
