package modele;
import java.awt.Color;
import java.io.Serializable;


/**
 * Classe modelisant un segment, utilisee pour le trace a main levee
 */
public class Segment implements Serializable {

	
	/**
	 * Points de depart et de depart du segment
	 */
	private Point dep, arr;
	
	
	/**
	 * Couleur du segment
	 */
	private Color couleur;
	
	
	/**
	 * Constructeur d'un segment
	 * 
	 * @param d
	 * 			Point de depart du segment
	 * 
	 * @param a
	 * 			Point d'arrivee du segment
	 * 
	 * @param c
	 * 			Couleur du segment
	 */
	public Segment(Point d, Point a, Color c){
		
		this.dep = d;
		this.arr = a;
		this.couleur = c;	
	
	}


	/**
	 * Methode renvoyant le point de depart
	 * 
	 * @return
	 * 			Point de depart
	 */
	public Point getDep() {
		return dep;
	}


	/**
	 * Methode modifiant le point de depart
	 * 
	 * @param dep
	 * 			Nouveau point de depart
	 */
	public void setDep(Point dep) {
		this.dep = dep;
	}


	/**
	 * Methode renvoyant le point d'arrivee
	 * 
	 * @return
	 * 			Point d'arrivee
	 */
	public Point getArr() {
		return arr;
	}


	/**
	 * Methode modifiant le point d'arrivee
	 * 
	 * @param arr
	 * 			Nouveau point d'arrivee
	 */
	public void setArr(Point arr) {
		this.arr = arr;
	}


	/**
	 * Methode renvoyant la couleur du segment
	 * 
	 * @return
	 * 			Couleur du segment
	 */
	public Color getCouleur() {
		return couleur;
	}
	
}
