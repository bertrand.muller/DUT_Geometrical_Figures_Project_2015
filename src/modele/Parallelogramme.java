package modele;


/**
 * Classe modelisant un parallelogramme
 */
public class Parallelogramme extends Quadrilatere {

	
	/**
	 * Constructeur d'un parallelogramme
	 */
	public Parallelogramme() {
		super();
	}
	
	
	@Override
	/**
	 * Methode retournant le nombre de points de saisie
	 * 
	 * @return
	 * 			Nombre de clics
	 */
	public int nbClics() {

		return 4;

	}

	
	@Override
	/**
	 * Methode permettant d'effectuer une transformation 
	 * des coordonnees des points de memorisation du parallelogramme
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 * 
	 * @param indice
	 * 			Indice dans tab_mem du point a modifier
	 */
	public void transformation(int dx, int dy, int indice) {
		
		
		boolean incremY = false;
				
		
		tab_mem[indice].incrementerX(dx);
		
		
		//On teste que les points restent dans la zone de dessin
		if((tab_mem[indice].rendreY() > 105)){
		
			tab_mem[indice].incrementerY(dy);
			incremY = true;
		}
		
		
		//Test permettant de modifier les autres points en consequence, en fonction de l'indice 
		switch (indice) {

		case 0:

			tab_mem[1].incrementerX(dx);

			if (incremY) {
				tab_mem[1].incrementerY(dy);
			}
			break;

		case 1:
			
			tab_mem[0].incrementerX(dx);

			if (incremY) {
				tab_mem[0].incrementerY(dy);
			}

			break;

		case 2:

			tab_mem[3].incrementerX(dx);

			if (incremY) {
				tab_mem[3].incrementerY(dy);
			}

			break;

		case 3:
			
			tab_mem[2].incrementerX(dx);

			if (incremY) {
				tab_mem[2].incrementerY(dy);
			}
			break;

		}
		
		super.transformation(dx, dy, indice);
	
	}

}
