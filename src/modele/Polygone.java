package modele;

import java.awt.Graphics;
import java.awt.Polygon;


/**
 * Classe abstraite etant la super classe de tous les polygones
 */
public abstract class Polygone extends FigureColoree {

	
	/**
	 * Polygone JAVA
	 */
	private Polygon p;
	
	
	/**
	 * Constructeur d'un polygone
	 */
	public Polygone() {
		p = new Polygon();
	}
	
	
	/**
	 * Methode affichant un polygone (drawpolygon)
	 * 
	 * @param g
	 * 			Environnement graphique
	 */
	public void affiche(Graphics g) {
		super.affiche(g);
		g.drawPolygon(p);
	}
	
	
	@Override
	/**
	 * Methode abstraite retournant le nombre de points de memorisation
	 * 
	 * @return
	 * 			Nombre de points
	 */
	public abstract int nbPoints();

	
	@Override
	/**
	 * Methode retournant un booleen indiquant si les coordonnees
	 * passees en parametre se trouve a l'interieur du polygone
	 * 
	 * @param x
	 * 			Abscisse du point
	 * 
	 * @param y
	 * 			Ordonnee du point
	 * 
	 * @return
	 * 			Boolean indiquant si le point se trouve a l'interieur du polygone
	 */
	public boolean estDedans(int x, int y) {
		
		boolean dedans = p.contains(x,y);
		
		if(!dedans) {
			if(carreDeSelection(x, y) != -1) {
				dedans = true;
			}
		}
		
		return dedans;
		
	}

	
	@Override
	/**
	 * Methode modifiant le polygone conformement a un ensemble de points de saisie
	 * 
	 * @param ps
	 * 		Tableau contenant les nouveaux points de saisie	 	
	 */
	public void modifierPoints(Point[] ps){
		
		tab_mem = ps;
		
		//On parcourt l'ensemble des points de saisie et on les ajoute au polygone
		for(int i=0; i < ps.length; i++) {
			p.addPoint(ps[i].rendreX(), ps[i].rendreY());
		}

	}
	
	
	@Override
	/**
	 * Methode permettant d'effectuer une translation des coordonnees
	 * des points de memorisation du polygone
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 */
	public void translation(int dx, int dy) {
		
		
		int minY = tab_mem[0].rendreY();
		
		
		//On parcourt l'ensemble des points de memorisation du polygone
		for(int i = 1; i < tab_mem.length; i++){
			
			if(tab_mem[i].rendreY() < minY){
				
				minY = tab_mem[i].rendreY();
				
			}
			
			
		}
		
		
		
		if((minY + dy < 105)){
		
			dy = 0;
		}
		
		
		super.translation(dx, dy);
		p.translate(dx, dy);
	}
	
	
	@Override
	/**
	 * Methode permettant d'effectuer une transformation 
	 * des coordonnees des points de memorisation
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 * 
	 * @param indice
	 * 			Indice dans tab_mem du point a modifier
	 */
	public void transformation(int dx, int dy, int indice) {
		
		Polygon new_poly = new Polygon();
		
		
		//On parcourt l'ensemble des points de memorisation du polygone
		for(int i = 0; i < tab_mem.length; i++){
			
			new_poly.addPoint(tab_mem[i].rendreX(), tab_mem[i].rendreY());	
			
		}
		
		p = new_poly;
		
	}

	
	@Override
	/**
	 * Methode retournant le nombre de points dont a besoin,
	 * en general, pour la saisie d'un polygone
	 * 
	 * @return
	 * 			Nombre de clics
	 */
	public int nbClics(){
		
		return 4;
		
	}

}
