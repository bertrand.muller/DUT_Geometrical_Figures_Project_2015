package modele;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;


/**
 * Classe abstraite qui modelise n'importe quelle figure geometrique visualisable et manipulable 
 */
public abstract class FigureColoree implements Serializable {

	
	/**
	 * Couleur de remplissage
	 */
	protected Color couleur;
	
	
	/**
	 * Booleen indiquant si la figure est selectionnee
	 */
	private boolean selected;
	
	
	/**
	 * Tableau des points de memorisation de la figure
	 */
	protected Point[] tab_mem;
	
	
	/**
	 * Constante definissant la taille des carres de selection
	 */
	private static int TAILLE_CARRE_SELECTION = 5;
	
	
	/**
	 * Constante definissant la taille de la peripherie des carres de selection
	 */
	private static int PERIPHERIE_CARRE_SELECTION = 10;
	
	
	/**
	 * Constante definissant une tolerance sur les carres de selection
	 */
	protected static int TOLERANCE_CARRE_SELECTION = 20;
	
	
	/**
	 * Constructeur d'une figure coloree
	 */
	public FigureColoree() {
		couleur = Color.BLACK;
		selected = false;
		tab_mem = new Point[nbPoints()]; 
	}
	
	
	/**
	 * Methode d'affichage de la figure
	 * Si la figure est selectionnee des carres sont dessines autour des points de memorisation
	 * 
	 * @param g
	 * 			Environnement graphique
	 */
	public void affiche(Graphics g) {
		
		g.setColor(couleur);
		
		
		//On teste s'il s'agit d'une instance de Polygone
		if(this instanceof Polygone) {
			
			int[] tmpX = new int[nbPoints()];
			int[] tmpY = new int[nbPoints()];
			
			for(int i= 0; i < tab_mem.length; i++) {
				tmpX[i] = tab_mem[i].rendreX();
				tmpY[i] = tab_mem[i].rendreY();
			}
			
			g.fillPolygon(tmpX,tmpY,tab_mem.length);
			
		} else {
			
			//On teste s'il s'agit d'une instance de Cercle
			if(this instanceof Cercle) {
				g.fillOval((int)(tab_mem[0].rendreX()-(tab_mem[0].distance(tab_mem[1]))), (int)(tab_mem[0].rendreY()-(tab_mem[0].distance(tab_mem[1]))), (int)(tab_mem[0].distance(tab_mem[1])*2), (int)tab_mem[0].distance(tab_mem[1])*2);
			
			//On teste s'il s'agit d'une instance de Ellipse
			} else {
				g.fillOval(tab_mem[3].rendreX(), tab_mem[0].rendreY(), (int)tab_mem[3].distance(tab_mem[1]), (int)tab_mem[0].distance(tab_mem[2]));
			}
		}
		
		
		//On teste si la figure est selectionnee
		if (this.selected) {

			
			//On parcourt l'ensemble des points de memorisation
			for(int i =0; i < tab_mem.length;i++){
				
				g.setColor(Color.BLACK);
				
				
				Point Pcour = tab_mem[i];
				
				
				//on affiche des carres au niveau des points de memorisation
				g.fillRect(Pcour.rendreX() - TAILLE_CARRE_SELECTION,
						Pcour.rendreY() - TAILLE_CARRE_SELECTION,
						PERIPHERIE_CARRE_SELECTION, PERIPHERIE_CARRE_SELECTION);

			}
			
		}
	
	}
	
	
	/**
	 * Methode detectant un point se trouvant pres d'un carre de selection
	 * 
	 * @param x
	 * 			Abscisse d'un clic de souris
	 * 
	 * @param y
	 * 			Ordonnee d'un clic de souris
	 * 
	 * @return
	 * 			Indice dans tab_mem du point se trouvant pres d'un carre de selection
	 */
	public int carreDeSelection(int x, int y) {
		
		int i = 0;
		int indice = -1;
		boolean arret = false;
		Point clic = new Point(x,y);
		
		
		//On parcourt l'ensemble des points de memorisation
		while((i < tab_mem.length) && (!arret)) {
			
			
			//On s'arrete quand on a trouve un carre de selection pres du clic de souris
			if(tab_mem[i].distance(clic) <= PERIPHERIE_CARRE_SELECTION*2) {
				arret = true;
				indice = i;
			}
			
			i++;
			
		}
		
		return indice;
		
	}
	
	
	/**
	 * Methode permettant de changer la couleur de la figure
	 * 
	 * @param c
	 * 			Nouvelle couleur
	 */
	public void changerCouleur(Color c) {
		
		couleur = c;
		
	}
	
	
	/**
	 * Methode deselectionant la figure
	 */
	public void deSelectionne() {
		
		selected = false;
		
	}
	
	
	/**
	 * Methode selectionnant le figure
	 */
	public void selectionne() {
		
		selected = true;
		
	}
	
	
	/**
	 * Methode permettant d'effectuer une transformation 
	 * des coordonnees des points de memorisation
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 * 
	 * @param indice
	 * 			Indice dans tab_mem du point a modifier
	 */
	public void transformation(int dx, int dy, int indice) {
		
	}
	
	
	/**
	 * Methode permettant d'effectuer une translation des coordonnees
	 * des points de memorisation de la figure
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 */
	public void translation(int dx, int dy) {
		
		//On teste que le point superieur reste dans la zone de dessin par rapport a l'ordonnee
		if((tab_mem[0].rendreY() + dy < 100+TAILLE_CARRE_SELECTION)){
			dy = 0;
		}
		

		//On parcourt l'ensemble des points de memorisation
		for(int i=0; i < tab_mem.length; i++) {

			tab_mem[i].translation(dx, dy);
		
		}
		
	}
	
	
	/**
	 * Methode abstraite retournant le nombre de points de memorisation
	 * 
	 * @return
	 * 			Nombre de points
	 */
	public abstract int nbPoints();
	
	
	/**
	 * Methode retournant un booleen indiquant si les coordonnees
	 * passees en parametre se trouve a l'interieur dela figure
	 * 
	 * @param x
	 * 			Coordonnee en abscisse
	 * 
	 * @param y
	 * 			Coordonne en ordonnee
	 * 
	 * @return
	 * 			Booleen indiquant si les coordonnees du point sont a l'interieur de la figure	
	 */
	public abstract boolean estDedans(int x, int y);
	
	
	/**
	 * Methode abstraite permettant de modifier les points
	 * de memorisation a partir de points de saisie
	 * 
	 * @param ps
	 * 			Tableau contenant les nouveaux points de saisie
	 */
	public abstract void modifierPoints(Point[] ps);
	
	
	/**
	 * Methode abstraite retournant le nombre de points de saisie
	 * 
	 * @return
	 * 			Nombre de clics
	 */
	public abstract int nbClics();
	
}
