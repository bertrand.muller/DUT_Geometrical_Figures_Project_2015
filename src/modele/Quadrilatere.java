package modele;


/**
 * Classe modelisant un quadrilatere
 */
public class Quadrilatere extends Polygone{

	
	@Override
	/**
	 * Methode retournant le nombre de points de memorisation d'un quadrilatere
	 */
	public int nbPoints() {
	
		return 4;
	}

	
	@Override
	/**
	 * Methode retournant le nombre de points dont a besoin,
	 * en general, pour la saisie d'un quadrilatere
	 * 
	 * @return
	 * 			Nombre de clics
	 */
	public int nbClics() {
		
		return 4;
	}

}
