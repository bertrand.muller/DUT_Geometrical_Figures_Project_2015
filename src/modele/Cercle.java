package modele;

import java.awt.Graphics;


/**
 * Classe modelisant un cercle
 */
public class Cercle extends FigureColoree {
	
	
	/**
	 * Constructeur d'un cercle
	 */
	public Cercle() {
		super();
	}
	
	
	/**
	 * Methode affichant un cercle (drawOval)
	 * 
	 * @param g
	 * 			Environnement graphique
	 */
	public void affiche(Graphics g) {
		super.affiche(g);
		g.drawOval((int)(tab_mem[0].rendreX()-(tab_mem[0].distance(tab_mem[1]))), (int)(tab_mem[0].rendreY()-(tab_mem[0].distance(tab_mem[1]))), (int)(tab_mem[0].distance(tab_mem[1])*2), (int)tab_mem[0].distance(tab_mem[1])*2);
	}

	
	@Override
	/**
	 * Methode retournant le nombre de points de memorisation d'un cercle
	 * 
	 * @return
	 * 		Nombre de points
	 */
	public int nbPoints() {
		return 2;
	}

	
	@Override
	/**
	 * Methode retournant un booleen indiquant si les coordonnees 
	 * passees en parametre se trouve a l'interieur du cercle
	 *
	 * @param x
	 * 			Coordonnee en abscisse
	 * 
	 * @param y
	 * 			Coordonnee en ordonnee
	 */
	public boolean estDedans(int x, int y) {
		
		boolean dedans = false;
		
		if(tab_mem[0].distance(new Point(x,y)) < tab_mem[0].distance(tab_mem[1])) {
			dedans = true;
		}
		
		if(!dedans) {
			if(carreDeSelection(x, y) != -1) {
				dedans = true;
			}
		}
		
		return dedans;
	}

	
	@Override
	/**
	 * Methode modifiant le cercle conformement aux deux points de saisie
	 * 
	 * @param ps
	 * 			Tableau contenant les nouveaux points de saisie
	 */
	public void modifierPoints(Point[] ps) {
		
		tab_mem = ps;
		
	}
	
	
	@Override
	/**
	 * Methode permettant d'effectuer une translation des coordonnees
	 * des points de memorisation et du centre et du cercle
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 */
	public void translation(int dx, int dy) {
		
		if(tab_mem[1].rendreY()+dy < 105) {
			dy = 0;
		}
		
		super.translation(dx, dy);
	}
	
	
	@Override
	/**
	 * Methode permettant d'effectuer une transformation des coordonnees
	 * des points de memorisation du cercle
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 * 
	 * @param indice
	 * 			Indice dans tab_mem du point a modifier
	 */
	public void transformation(int dx, int dy, int indice) {
		
		boolean incremY = false;
		
		tab_mem[indice].incrementerX(dx);
		
		//On teste que les points restent dans la zone de dessin
		if(tab_mem[indice].rendreY()+dy > 105) {
			tab_mem[indice].incrementerY(dy);
			incremY  =true;
		}
		
		
		//Test permettant de modifier les autres points en consequence, en fonction de l'indice 
		switch(indice) {
		
		case 0:
			
			tab_mem[1].incrementerX(dx);
			
			if(incremY) {
				tab_mem[1].incrementerY(dy);
			}
			
			break;
			
		case 1:
			
			tab_mem[0].incrementerX(dx);
			break;
		}
		
	}

	
	@Override
	/**
	 * Methode retournant le nombre de points 
	 * dont on a besoin pour la saisie d'un cercle
	 * 
	 * @return
	 * 			Nombre de clics
	 */
	public int nbClics() {
		return 2;
	}

}

