package modele;

import java.awt.Graphics;
import java.awt.geom.Ellipse2D;


/**
 * Classe modelisant une ellipse
 */
public class Ellipse extends FigureColoree {
	
	
	/**
	 * Constructeur d'une ellipse
	 */
	public Ellipse() {
		super();
	}

	
	@Override
	/**
	 * Methode retournant le nombre de points de memorisation d'une ellipse
	 * 
	 * @return
	 * 		Nombre de points
	 */
	public int nbPoints() {
		return 4;
	}
	
	
	/**
	 * Methode affichant une ellipse (drawOval)
	 * 
	 * @param g
	 * 			Environnement graphique
	 */
	public void affiche(Graphics g) {
		super.affiche(g);
		g.drawOval(tab_mem[3].rendreX(), tab_mem[0].rendreY(), (int)tab_mem[3].distance(tab_mem[1]), (int)tab_mem[0].distance(tab_mem[2]));
	}

	
	@Override
	/**
	 * Methode retournant un booleen indiquant si les coordonnees 
	 * passees en parametre se trouve a l'interieur de l'ellipse
	 *
	 * @param x
	 * 			Coordonnee en abscisse
	 * 
	 * @param y
	 * 			Coordonnee en ordonnee
	 */
	public boolean estDedans(int x, int y) {
		
		//Creation d'une ellipse
		Ellipse2D.Double ellipse = new Ellipse2D.Double();
		Point tmp = new Point(tab_mem[0].rendreX(),tab_mem[1].rendreY());
		ellipse.setFrame(tab_mem[0].rendreX()-tmp.distance(tab_mem[1]), tab_mem[3].rendreY()-tmp.distance(tab_mem[0]), tab_mem[3].distance(tab_mem[1]), tab_mem[0].distance(tab_mem[2]));
		
		boolean dedans = ellipse.contains(x,y);
		
		if(!dedans) {
			if(carreDeSelection(x, y) != -1) {
				dedans = true;
			}
		}
		
		return dedans;
	}

	
	@Override
	/**
	 * Methode modifiant l'ellipse conformement aux points de saisie
	 * 
	 * @param ps
	 * 			Tableau contenant les nouveaux points de saisie
	 */
	public void modifierPoints(Point[] ps) {
		
		tab_mem = ps;

	}

	
	@Override
	/**
	 * Methode retournant le nombre de points 
	 * dont on a besoin pour la saisie d'une ellipse
	 * 
	 * @return
	 * 			Nombre de clics
	 */
	public int nbClics() {
		return 3;
	}
	
	
	@Override
	/**
	 * Methode permettant d'effectuer une transformation des coordonnees
	 * des points de memorisation de l'ellipse
	 * 
	 * @param dx
	 * 			Deplacement sur l'axe des abscisses
	 * 
	 * @param dy
	 * 			Deplacement sur l'axe des ordonnees
	 * 
	 * @param indice
	 * 			Indice dans tab_mem du point a modifier
	 */
	public void transformation(int dx, int dy, int indice) {

		boolean incremY = false;
		
		tab_mem[indice].incrementerX(dx);
		
		//On teste que les points restent dans la zone de dessin
		if(tab_mem[indice].rendreY()+dy > 105) {
			tab_mem[indice].incrementerY(dy);
			incremY = true;
		}
		
		
		//Test permettant de modifier les autres points en consequence, en fonction de l'indice 
		switch(indice) {
		
		case 0:
			
			tab_mem[1].incrementerX(dx);
			tab_mem[2].incrementerX(dx);
			tab_mem[3].incrementerX(dx);
			
			if(incremY) {
				tab_mem[2].incrementerY(-dy);
			}
			
			break;
			
		case 1:
			
			tab_mem[3].incrementerX(-dx);
			
			if(incremY) {
				tab_mem[0].incrementerY(dy);
				tab_mem[2].incrementerY(dy);
				tab_mem[3].incrementerY(dy);
			}
			
			break;
			
		case 2:
			
			tab_mem[0].incrementerX(dx);
			tab_mem[1].incrementerX(dx);
			tab_mem[3].incrementerX(dx);
			
			if(incremY) {
				
				if(tab_mem[0].rendreY()-dy > 105) {
					tab_mem[0].incrementerY(-dy);
				} else {
					tab_mem[2].incrementerY(-dy);
				}
				
			}
			
			break;
		
		case 3:
			
			tab_mem[1].incrementerX(-dx);
			
			if(incremY) {
				tab_mem[0].incrementerY(dy);
				tab_mem[1].incrementerY(dy);
				tab_mem[2].incrementerY(dy);
			}
			
			break;
			
		}
		
		
		//Test pour l'inversion des points en ordonnee
		if(tab_mem[0].rendreY() >= tab_mem[2].rendreY()) {
			Point tmp = tab_mem[0];
			tab_mem[0] = tab_mem[2];
			tab_mem[2] = tmp;
		}
		
		
		//Test pour l'inversion des points en abscisse
		if(tab_mem[1].rendreX() <= tab_mem[3].rendreX()) {
			Point tmp = tab_mem[1];
			tab_mem[1] = tab_mem[3];
			tab_mem[3] = tmp;
		}
		
	}

}
