# DUT - _Geometrical Figures_ 
 
Indices Couleurs :

     - 0 --> Rouge

     - 1 --> Vert

     - 2 --> Bleu

     - 3 --> Jaune

     - 4 --> Noir

     - 5 --> Violet

     - 6 --> Rose

     - 7 --> Orange


Indices Figures :

     - 0 --> Rectangle

     - 1 --> Triangle

     - 2 --> Carre

     - 3 --> Losange

     - 4 --> Figure Quelconque

     - 5 --> Parallelogramme


Blocage des figures dans la zone de dessin à 45 pixels (40 + TAILLE_CARRE_SELECTION) à partir du haut